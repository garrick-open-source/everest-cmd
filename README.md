# Everest Offer CMD Application

## Quick Summary

Project is built using node js with typescript.

## Getting started

Tools required

- [NodeJs](https://nodejs.org/en/) version 16.15.0
- [yarn](https://yarnpkg.com/)

## Installation

Clone this repo and cd to your directory , run

```bash
yarn
```

As project is build using typescript, to build application

```bash
yarn build
```

To build and compile in real time, run

```bash
yarn tsc-watch
```

Make sure build is run at the very first stage of project init to create database json file

## Test

Jest is used as testing framework.

To run

```bash
yarn test
```

## Usage

### Get Offers

```bash
yarn everest:get-offers
```

### Edit Offer

```bash
yarn everest:edit-offer
```

### Add Offer

```bash
yarn everest:add-offer
```

### Delete Offer

```bash
yarn everest:delete-offer
```

### Calculate Delivery Cost

```bash
yarn everest:delivery-cost-estimation
```

## Future Enhancement

- As this is an offline application, json object file is used as the offline database. To further optimized, we can use database machine to improve the performance and scalability if needed such as NoSql- MongoDB

## Demo Recording

https://www.loom.com/share/9b66eaac1c2c49c99b9e39b2d6a6488d
