export interface IPackage {
  id: string;
  weight: number;
  distance: number;
  offerId?: string;
}
