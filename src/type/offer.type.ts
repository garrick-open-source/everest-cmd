export interface IOffers {
  [offerId: string]: IOffer;
}

export interface IOffer {
  discountPercentage: number;
  distance: {
    min: number;
    max: number;
  };
  weight: {
    min: number;
    max: number;
  };
}
