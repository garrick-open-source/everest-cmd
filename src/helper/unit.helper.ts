import _ from "lodash";
import validator from "validator";

export function isValidPercentage(percentage: string | number): boolean {
  if (_.isString(percentage)) {
    // Check non numeric string
    if (!validator.isNumeric(percentage)) {
      return false;
    }

    // Convert to number
    percentage = _.toNumber(percentage);
  }

  return percentage >= 0 && percentage <= 100;
}

export function isValidDistance(distance: string | number): boolean {
  if (_.isString(distance)) {
    // Check non numeric string
    if (!validator.isNumeric(distance)) {
      return false;
    }

    // Convert to number
    distance = _.toNumber(distance);
  }

  return distance >= 0;
}

export function isValidWeight(weight: string | number): boolean {
  if (_.isString(weight)) {
    // Check non numeric string
    if (!validator.isNumeric(weight)) {
      return false;
    }

    // Convert to number
    weight = _.toNumber(weight);
  }

  return weight >= 0;
}
