import { IOffer } from "@src/type/offer.type";

export function isOfferValid(offer: IOffer): boolean {
  // Check valid discount percentage
  if (offer.discountPercentage < 0 || offer.discountPercentage > 100) {
    return false;
  }

  // Check valid distance range
  if (offer.distance.min < 0 || offer.distance.max < 0) {
    return false;
  }
  if (offer.distance.min >= offer.distance.max) {
    return false;
  }

  // Check valid distance range
  if (offer.weight.min < 0 || offer.weight.max < 0) {
    return false;
  }
  if (offer.weight.min >= offer.weight.max) {
    return false;
  }

  return true;
}
