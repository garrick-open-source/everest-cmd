import _ from "lodash";
import winston, { format } from "winston";

/**
 * Local console log format
 * @param param0 winston.Logform.TransformableInfo
 * @returns
 */
function localLogPrintFormat({
  message,
  level,
  tag,
  subTag,
  ...optionArgs
}: winston.Logform.TransformableInfo) {
  if (_.isObject(message)) {
    message = JSON.stringify(message, null, 2);
  }

  let logString = `${level} [${tag}] ${subTag ? `(${subTag})` : ""}: ${message} `;

  if (!_.isEmpty(optionArgs)) {
    logString += `${JSON.stringify(optionArgs, null, 2)}`;
    return logString;
  }

  return logString;
}

/**
 * * Main logging function
 * At local we will log message in string format
 */
export function initLogger(tag: string, extraTags?: Object): winston.Logger {
  return winston.createLogger({
    level: "debug",
    defaultMeta: { tag: tag, ...extraTags },
    transports: [
      new winston.transports.Console({
        format: format.combine(
          format.colorize(),
          format.splat(),
          format.simple(),
          format.printf(localLogPrintFormat)
        ),
      }),
    ],
    exitOnError: false,
  });
}

/**
 * Sub tag format for logger
 * @param subTag Sub tag id
 * @param args Args
 * @returns
 */
export const subTag = (
  subTag: string,
  args?: Object
): Readonly<{ subTag: string } & { [arg: string]: any }> => {
  return { subTag, ...args };
};
