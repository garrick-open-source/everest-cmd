import "module-alias/register";
import * as OfferHelper from "@src/helper/offer.helper";

describe("isOfferValid method", () => {
  test("Test valid offer", async () => {
    expect(
      OfferHelper.isOfferValid({
        discountPercentage: 0,
        weight: { min: 0, max: 100 },
        distance: { min: 0, max: 100 },
      })
    ).toBe(true);

    expect(
      OfferHelper.isOfferValid({
        discountPercentage: 100,
        weight: { min: 0, max: 100 },
        distance: { min: 0, max: 100 },
      })
    ).toBe(true);

    expect(
      OfferHelper.isOfferValid({
        discountPercentage: 50,
        weight: { min: 0, max: 100 },
        distance: { min: 0, max: 100 },
      })
    ).toBe(true);
  });

  test("Test invalid offer", async () => {
    expect(
      OfferHelper.isOfferValid({
        discountPercentage: -5,
        weight: { min: 0, max: 100 },
        distance: { min: 0, max: 100 },
      })
    ).toBe(false);

    expect(
      OfferHelper.isOfferValid({
        discountPercentage: -100,
        weight: { min: 0, max: 100 },
        distance: { min: 0, max: 100 },
      })
    ).toBe(false);

    expect(
      OfferHelper.isOfferValid({
        discountPercentage: 50,
        weight: { min: -5, max: 100 },
        distance: { min: 0, max: 100 },
      })
    ).toBe(false);

    expect(
      OfferHelper.isOfferValid({
        discountPercentage: 50,
        weight: { min: 0, max: 100 },
        distance: { min: -10, max: 100 },
      })
    ).toBe(false);
  });
});
