import "module-alias/register";
import * as UnitHelper from "@src/helper/unit.helper";

describe("isValidPercentage method", () => {
  test("Test valid percentage", async () => {
    expect(UnitHelper.isValidPercentage(100)).toBe(true);
    expect(UnitHelper.isValidPercentage(0)).toBe(true);
    expect(UnitHelper.isValidPercentage("0")).toBe(true);
    expect(UnitHelper.isValidPercentage("100")).toBe(true);
  });

  test("Test valid invalid percentage", async () => {
    expect(UnitHelper.isValidPercentage(1000)).toBe(false);
    expect(UnitHelper.isValidPercentage(-10)).toBe(false);
    expect(UnitHelper.isValidPercentage("1000")).toBe(false);
    expect(UnitHelper.isValidPercentage("-10")).toBe(false);
  });
});

describe("isValidDistance method", () => {
  test("Test valid distance", async () => {
    expect(UnitHelper.isValidDistance(100)).toBe(true);
    expect(UnitHelper.isValidDistance(0)).toBe(true);
    expect(UnitHelper.isValidDistance("0")).toBe(true);
    expect(UnitHelper.isValidDistance("100")).toBe(true);
  });

  test("Test invalid distance", async () => {
    expect(UnitHelper.isValidDistance(-10)).toBe(false);
    expect(UnitHelper.isValidDistance("-10")).toBe(false);
  });
});

describe("isValidWeight method", () => {
  test("Test valid weight", async () => {
    expect(UnitHelper.isValidWeight(100)).toBe(true);
    expect(UnitHelper.isValidWeight(0)).toBe(true);
    expect(UnitHelper.isValidWeight("0")).toBe(true);
    expect(UnitHelper.isValidWeight("100")).toBe(true);
  });

  test("Test invalid weight", async () => {
    expect(UnitHelper.isValidWeight(-10)).toBe(false);
    expect(UnitHelper.isValidWeight("-10")).toBe(false);
  });
});
