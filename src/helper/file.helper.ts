import fs from "fs";
import { promisify } from "util";
import { initLogger } from "./logger.helper";
const logger = initLogger("file-helper");

const writeFileAsync = promisify(fs.writeFile);

export async function createFile({
  folderPath,
  fileName,
  data,
}: {
  folderPath: string;
  fileName: string;
  data: Buffer;
}) {
  if (!fs.existsSync(`${folderPath}`)) {
    fs.mkdirSync(`${folderPath}`);
    logger.info(`Created folder ${folderPath}`);
  }

  await writeFileAsync(folderPath + fileName, Buffer.from(data), "utf-8");
  logger.info(`Created file ${folderPath + fileName}`);
}
