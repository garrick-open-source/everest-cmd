import "module-alias/register";
import * as OfferService from "@src/service/offer.service";
import fs from "fs";
import _ from "lodash";

jest.mock("fs");
// Setup test data
const MockedOffer = {
  OFR001: {
    discountPercentage: 10,
    distance: { min: 0, max: 200 },
    weight: { min: 70, max: 200 },
  },
  OFR002: {
    discountPercentage: 7,
    distance: { min: 50, max: 150 },
    weight: { min: 100, max: 250 },
  },
  OFR003: {
    discountPercentage: 5,
    distance: { min: 50, max: 250 },
    weight: { min: 10, max: 150 },
  },
};
//@ts-ignore
fs.__setMockFiles(OfferService.OFFER_JSON_FILE, Buffer.from(JSON.stringify(MockedOffer)));

describe("getOfferById method", () => {
  const offers = OfferService.getAllOffer();

  test("Test valid offer", async () => {
    expect(OfferService.getOfferById(offers, "OFR001")).toStrictEqual({
      discountPercentage: 10,
      distance: { min: 0, max: 200 },
      weight: { min: 70, max: 200 },
    });
  });

  test("Test invalid offer", async () => {
    expect(() => OfferService.getOfferById(offers, "HELLO")).toThrowError("Offer doesn't exist");
  });
});

describe("editOffer method", () => {
  test("Edit offer", async () => {
    const newOffer = OfferService.editOffer({
      offer: {
        id: "OFR001",
        discountPercentage: 88,
        distance: { min: 60, max: 70 },
        weight: { min: 60, max: 70 },
      },
    });

    expect(newOffer).toStrictEqual({
      id: "OFR001",
      discountPercentage: 88,
      distance: { min: 60, max: 70 },
      weight: { min: 60, max: 70 },
    });
  });
});

describe("addOffer method", () => {
  test("Add offer", async () => {
    const newOffer = OfferService.addOffer({
      offer: {
        id: "OFR005",
        discountPercentage: 88,
        distance: { min: 60, max: 70 },
        weight: { min: 60, max: 70 },
      },
    });

    expect(newOffer).toStrictEqual({
      id: "OFR005",
      discountPercentage: 88,
      distance: { min: 60, max: 70 },
      weight: { min: 60, max: 70 },
    });
  });
});

describe("isPackageValidForOffer method", () => {
  test("Test valid offer", async () => {
    const offers = OfferService.getAllOffer();

    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 75, distance: 5 },
      })
    ).toBe(true);

    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 70, distance: 5 },
      })
    ).toBe(true);

    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 200, distance: 5 },
      })
    ).toBe(true);

    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 75, distance: 0 },
      })
    ).toBe(true);
    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 75, distance: 200 },
      })
    ).toBe(true);
  });

  test("Test invalid offer", async () => {
    const offers = OfferService.getAllOffer();

    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 69, distance: 5 },
      })
    ).toBe(false);
    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 201, distance: 5 },
      })
    ).toBe(false);
    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 71, distance: -0.5 },
      })
    ).toBe(false);
    expect(
      OfferService.isPackageValidForOffer(offers["OFR001"], {
        package: { weight: 71, distance: 201 },
      })
    ).toBe(false);
  });
});

describe("calculateDeliveryCost method", () => {
  const offers = OfferService.getAllOffer();

  test("Test weight less than zero", async () => {
    expect(() =>
      OfferService.calculateDeliveryCost(offers, {
        baseDeliveryCost: 100,
        package: { weight: -0.5, distance: 10 },
      })
    ).toThrowError("Package weight can't less or equal to zero");
  });

  test("Test distance less than zero", async () => {
    expect(() =>
      OfferService.calculateDeliveryCost(offers, {
        baseDeliveryCost: 100,
        package: { weight: 100, distance: -0.5 },
      })
    ).toThrowError("Package distance can't less or equal to zero");
  });

  test("Test without offer", async () => {
    expect(
      OfferService.calculateDeliveryCost(offers, {
        baseDeliveryCost: 100,
        package: { weight: 5, distance: 5 },
      })
    ).toStrictEqual({ cost: 175, discountCost: 0 });
  });

  test("Test with invalid offer", async () => {
    expect(
      OfferService.calculateDeliveryCost(offers, {
        baseDeliveryCost: 100,
        package: { weight: 5, distance: 5, offerId: "HELLO" },
      })
    ).toStrictEqual({ cost: 175, discountCost: 0 });
  });

  test("Test with valid offer", async () => {
    expect(
      OfferService.calculateDeliveryCost(offers, {
        baseDeliveryCost: 100,
        package: { weight: 10, distance: 100, offerId: "OFR003" },
      })
    ).toStrictEqual({ cost: 665, discountCost: 35 });
  });
});
