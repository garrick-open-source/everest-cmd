import { createFile } from "@src/helper/file.helper";
import * as OfferHelper from "@src/helper/offer.helper";
import { IOffer, IOffers } from "@src/type/offer.type";
import fs from "fs";
import _ from "lodash";

export const OFFER_JSON_FILE = "data/offer.database.json";

/**
 * Check if offer database json file exist and init if it doesn't
 * @returns
 */
export function checkAndInitOfferDatabase() {
  if (fs.existsSync(OFFER_JSON_FILE)) {
    return;
  }

  // Default Sample Data
  const defaultOffer = {
    OFR001: {
      discountPercentage: 10,
      distance: { min: 0, max: 200 },
      weight: { min: 70, max: 200 },
    },
    OFR002: {
      discountPercentage: 7,
      distance: { min: 50, max: 150 },
      weight: { min: 100, max: 250 },
    },
    OFR003: {
      discountPercentage: 5,
      distance: { min: 50, max: 250 },
      weight: { min: 10, max: 150 },
    },
  };

  createFile({
    folderPath: "data/",
    fileName: "offer.database.json",
    data: Buffer.from(JSON.stringify(defaultOffer)),
  });
}

/**
 * Get offer
 */
export function getAllOffer(): IOffers {
  // Check if file exist
  if (!fs.existsSync(OFFER_JSON_FILE)) {
    throw new Error("Offer file doesn't exist");
  }

  const offerBuffer = fs.readFileSync(OFFER_JSON_FILE);
  const offer = <IOffers>JSON.parse(offerBuffer.toString());

  return offer;
}

/**
 * Get offer by offer id
 */
export function getOfferById(offer: IOffers, offerId: string): IOffer {
  if (!(offerId in offer)) {
    throw new Error("Offer doesn't exist");
  }

  return offer[offerId];
}

/**
 * Edit offer to json file
 */
export function editOffer(param: {
  offer: {
    id: string;
    discountPercentage: number;
    distance: {
      min: number;
      max: number;
    };
    weight: {
      min: number;
      max: number;
    };
  };
}): IOffer & { id: string } {
  const offers = getAllOffer();

  if (!(param.offer.id in offers)) {
    throw new Error("Offer ID does't not exist");
  }

  // Check valid offer
  if (
    !OfferHelper.isOfferValid({
      discountPercentage: param.offer.discountPercentage,
      distance: {
        min: param.offer.distance.min,
        max: param.offer.distance.max,
      },
      weight: {
        min: param.offer.weight.min,
        max: param.offer.weight.max,
      },
    })
  ) {
    throw new Error("Invalid offer");
  }

  // Update new data
  offers[param.offer.id] = {
    discountPercentage: param.offer.discountPercentage,
    distance: {
      min: param.offer.distance.min,
      max: param.offer.distance.max,
    },
    weight: {
      min: param.offer.distance.min,
      max: param.offer.distance.max,
    },
  };

  fs.writeFileSync(OFFER_JSON_FILE, JSON.stringify(offers));

  return { ...offers[param.offer.id], id: param.offer.id };
}

/**
 * Add offer to json file
 */
export function addOffer(param: {
  offer: {
    id: string;
    discountPercentage: number;
    distance: {
      min: number;
      max: number;
    };
    weight: {
      min: number;
      max: number;
    };
  };
}): IOffer & { id: string } {
  const offers = getAllOffer();

  if (param.offer.id in offers) {
    throw new Error("Offer ID exist");
  }

  // Check valid offer
  if (
    !OfferHelper.isOfferValid({
      discountPercentage: param.offer.discountPercentage,
      distance: {
        min: param.offer.distance.min,
        max: param.offer.distance.max,
      },
      weight: {
        min: param.offer.weight.min,
        max: param.offer.weight.max,
      },
    })
  ) {
    throw new Error("Invalid offer");
  }

  // Add new data
  offers[param.offer.id] = {
    discountPercentage: param.offer.discountPercentage,
    distance: {
      min: param.offer.distance.min,
      max: param.offer.distance.max,
    },
    weight: {
      min: param.offer.distance.min,
      max: param.offer.distance.max,
    },
  };

  fs.writeFileSync(OFFER_JSON_FILE, JSON.stringify(offers));

  return { ...offers[param.offer.id], id: param.offer.id };
}

/**
 * Delete offer by offer id
 */
export function deleteOffer(offerId: string) {
  const offer = getAllOffer();

  if (!(offerId in offer)) {
    throw new Error("Offer ID does't not exist");
  }

  delete offer[offerId];

  fs.writeFileSync(OFFER_JSON_FILE, JSON.stringify(offer));

  return true;
}

export function isOfferExistByOfferId(offers: IOffers, offerId: string): boolean {
  if (!(offerId in offers)) {
    return false;
  }

  return true;
}

export function isPackageValidForOffer(
  offer: IOffer,
  param: {
    package: {
      weight: number;
      distance: number;
    };
  }
): boolean {
  if (param.package.weight < offer.weight.min || param.package.weight > offer.weight.max) {
    return false;
  }

  if (param.package.distance < offer.distance.min || param.package.distance > offer.distance.max) {
    return false;
  }

  return true;
}

export function calculateDeliveryCost(
  offers: IOffers,
  param: {
    baseDeliveryCost: number;
    package: {
      weight: number;
      distance: number;
      offerId?: string;
    };
  }
): { cost: number; discountCost: number } {
  if (param.package.weight <= 0) {
    throw new Error("Package weight can't less or equal to zero");
  }

  if (param.package.distance <= 0) {
    throw new Error("Package distance can't less or equal to zero");
  }

  let totalCost = param.baseDeliveryCost + param.package.weight * 10 + param.package.distance * 5;

  if (!param.package.offerId || !isOfferExistByOfferId(offers, param.package.offerId)) {
    return { cost: totalCost, discountCost: 0 };
  }

  // Query offer details
  const offer = getOfferById(offers, param.package.offerId);

  // Check is package valid for the offer
  if (
    !isPackageValidForOffer(offer, {
      package: { weight: param.package.weight, distance: param.package.distance },
    })
  ) {
    return { cost: totalCost, discountCost: 0 };
  }

  // Minus discount
  const discountCost = totalCost * (offer.discountPercentage / 100);
  totalCost -= discountCost;

  return { cost: totalCost, discountCost: discountCost };
}
