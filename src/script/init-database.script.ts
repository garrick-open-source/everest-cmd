import "module-alias/register";
import * as OfferService from "@src/service/offer.service";

function initDatabase() {
  OfferService.checkAndInitOfferDatabase();
}

initDatabase();
