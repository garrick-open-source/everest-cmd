import "module-alias/register";
import inquirer from "inquirer";
import _ from "lodash";
import validator from "validator";
import { initLogger } from "@src/helper/logger.helper";
import * as OfferService from "@src/service/offer.service";
import * as UnitHelper from "@src/helper/unit.helper";

const logger = initLogger("delivery-cost-estimation");

// Preload cached offers
const offers = OfferService.getAllOffer();

const output: {
  baseDeliveryCost: number;
  totalPackages: number;
  packages: string[];
} = {
  baseDeliveryCost: 0,
  totalPackages: 0,
  packages: [],
};

function inquireBaseDeliveryCostAndTotalPackages() {
  return inquirer.prompt({
    type: "input",
    name: "base",
    validate: function (answer) {
      const inputList = answer.trim().replace(/\s\s+/g, " ").split(" ");
      if (inputList.length != 2) {
        return "Accept two argument only";
      }
      const [baseDeliveryCost, totalPackages] = inputList;

      // Validate input
      if (!validator.isNumeric(baseDeliveryCost) || _.toNumber(baseDeliveryCost) < 0) {
        return "base_delivery_cost must be a number and greater than zero";
      }
      if (!validator.isNumeric(totalPackages) || _.toNumber(totalPackages) < 0) {
        return "no_of_packages must be a number and greater than zero";
      }

      // Store base value
      output.baseDeliveryCost = _.toNumber(baseDeliveryCost);
      output.totalPackages = _.toNumber(totalPackages);
      return true;
    },
    message:
      "Enter based delivery cost and total number of packages - base_delivery_cost no_of_packages eg. 100 3",
  });
}

async function inquirePackages() {
  const packageNo = output.packages.length + 1;

  return inquirer
    .prompt({
      type: "input",
      name: "packages",
      message: `Enter package ${packageNo} detail - pkg_id pkg_weight_in_kg distance_in_km offer_code(optional) eg. PKG1 5 5 OFR001`,
      validate: function (answer) {
        const inputList = answer.trim().replace(/\s\s+/g, " ").split(" ");
        if (inputList.length > 4 || inputList.length < 3) {
          return "Accept three/four argument only";
        }
        const [packageId, weight, distance, offerId] = inputList;

        // Validate input
        if (!_.isString(packageId)) {
          return "Package ID must be string";
        }
        if (!UnitHelper.isValidWeight(weight)) {
          return "pkg_weight_in_kg must be a number and greater than zero";
        }
        if (!UnitHelper.isValidDistance(distance)) {
          return "distance_in_km must be a number and greater than zero";
        }

        try {
          const { cost, discountCost: discountPercentage } = OfferService.calculateDeliveryCost(
            offers,
            {
              baseDeliveryCost: output.baseDeliveryCost,
              package: {
                weight: weight,
                distance: distance,
                offerId: offerId,
              },
            }
          );

          output.packages.push(`${packageId} ${discountPercentage} ${cost}`);
        } catch (error) {
          return error.message;
        }

        return true;
      },
    })
    .then(async () => {
      if (output.packages.length < output.totalPackages) {
        await inquirePackages();
        return;
      } else {
        for (const _package of output.packages) {
          console.log(_package);
        }
        return;
      }
    });
}

async function main() {
  await inquireBaseDeliveryCostAndTotalPackages();
  await inquirePackages();
}

main()
  .then(() => {
    logger.info("Script run successfully");
  })
  .catch((error) => {
    logger.error(error.message);
  });
