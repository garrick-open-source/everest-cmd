import inquirer from "inquirer";
import _ from "lodash";
import "module-alias/register";
import { initLogger } from "@src/helper/logger.helper";
import * as OfferService from "@src/service/offer.service";
import * as UnitHelper from "@src/helper/unit.helper";

const logger = initLogger("add-offer");

function addOffer() {
  return inquirer.prompt([
    {
      type: "input",
      name: "offerId",
      message: "Enter Offer ID eg. OFR003",
      validate: (inputOfferId) => {
        const offers = OfferService.getAllOffer();

        if (inputOfferId in offers) {
          return "Offer id exist";
        }

        return true;
      },
    },
    {
      type: "input",
      name: "discountPercentage",
      message: "Enter discount percentage without % - eg. 50",
      validate: (inputDiscountPercentage) => {
        if (!UnitHelper.isValidPercentage(inputDiscountPercentage)) {
          return "Input must be a number , in percentage value - 0<= percentage <=100";
        }

        return true;
      },
    },
    {
      type: "input",
      name: "minDistance",
      message: "Enter min distance in km eg. 50",
      validate: (minDistance) => {
        if (!UnitHelper.isValidDistance(minDistance)) {
          return "Input must be a number and greater than zero";
        }

        return true;
      },
    },
    {
      type: "input",
      name: "maxDistance",
      message: "Enter max distance in km eg. 50",
      validate: (maxDistance, answer) => {
        if (!UnitHelper.isValidDistance(maxDistance)) {
          return "Input must be a number and greater than zero";
        }

        if (_.toNumber(answer.minDistance) >= _.toNumber(maxDistance)) {
          return "Min distance must smaller than max distance";
        }

        return true;
      },
    },
    {
      type: "input",
      name: "minWeight",
      message: "Enter min weight in  kg eg. 50",
      validate: (minWeight) => {
        if (!UnitHelper.isValidWeight(minWeight)) {
          return "Input must be a number and greater than zero";
        }

        return true;
      },
    },
    {
      type: "input",
      name: "maxWeight",
      message: "Enter max weight in kg eg. 50",
      validate: (maxWeight, answer) => {
        if (!UnitHelper.isValidWeight(maxWeight)) {
          return "Input must be a number and greater than zero";
        }

        if (_.toNumber(answer.minWeight) >= _.toNumber(maxWeight)) {
          return "Min input must smaller than max input";
        }

        return true;
      },
    },
  ]);
}

async function main() {
  const answer = await addOffer();

  OfferService.addOffer({
    offer: {
      id: answer.offerId,
      discountPercentage: _.toNumber(answer.discountPercentage),
      distance: { min: _.toNumber(answer.minDistance), max: _.toNumber(answer.maxDistance) },
      weight: {
        min: _.toNumber(answer.minWeight),
        max: _.toNumber(answer.maxWeight),
      },
    },
  });
}

main()
  .then(() => {
    logger.info("Script run successfully");
  })
  .catch((error) => {
    logger.error(error.message);
  });
