import _ from "lodash";
import "module-alias/register";
import { initLogger } from "@src/helper/logger.helper";
import * as OfferService from "@src/service/offer.service";

const logger = initLogger("get-offer");

function main() {
  const offers = OfferService.getAllOffer();
  logger.info(offers);
}

try {
  main();
} catch (error) {
  logger.error(error.message);
}
