import "module-alias/register";
import inquirer from "inquirer";
import _ from "lodash";
import "module-alias/register";
import { initLogger } from "@src/helper/logger.helper";
import * as OfferService from "@src/service/offer.service";

const logger = initLogger("delete-offer");

// Preload cached offers
const offers = OfferService.getAllOffer();

function addOffer() {
  return inquirer.prompt([
    {
      type: "text",
      name: "offerId",
      message: "Enter Offer ID to delete eg. OFR003",
      validate: (inputOfferId) => {
        if (!(inputOfferId in offers)) {
          return "Offer id does not exist";
        }

        return true;
      },
    },
  ]);
}

async function main() {
  const { offerId } = await addOffer();

  OfferService.deleteOffer(offerId);
}

main()
  .then(() => {
    logger.info("Script run successfully");
  })
  .catch((error) => {
    logger.error(error.message);
  });
