const fs: any = jest.createMockFromModule("fs");

const mockFiles: { [filePath: string]: any } = {};

function __setMockFiles(filePath: string, content: any) {
  mockFiles[filePath] = content;
}

// A custom version of `readdirSync` that reads from the special mocked out
// file list set via __setMockFiles
function readdirSync(directoryPath: string) {
  return mockFiles[directoryPath] || [];
}

function existsSync(directoryPath: string) {
  return directoryPath in mockFiles;
}

function readFileSync(directoryPath: string) {
  return mockFiles[directoryPath] || [];
}

fs.__setMockFiles = __setMockFiles;
fs.readdirSync = readdirSync;
fs.existsSync = existsSync;
fs.readFileSync = readFileSync;

export default fs;
